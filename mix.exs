defmodule WampexTestSuite.MixProject do
  use Mix.Project

  def project do
    [
      app: :wampex_test_suite,
      version: "0.1.0",
      elixir: "~> 1.17",
      elixirc_paths: elixirc_paths(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test,
        all_tests: :test
      ],
      test_coverage: [tool: ExCoveralls]
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp elixirc_paths, do: ["lib", "test/support"]

  def deps do
    [
      {:excoveralls, "~> 0.18", runtime: false}
    ] ++ deps(Mix.env())
  end

  defp deps(:dev) do
    [
      {:cluster_kv, path: "../cluster_kv", override: true},
      {:wampex, path: "../wampex", override: true},
      {:wampex_client, path: "../wampex_client"},
      {:wampex_router, path: "../wampex_router"}
    ]
  end

  defp deps(:test) do
    [
      {:cluster_kv,
       git: "https://gitlab.com/entropealabs/cluster_kv.git", tag: "master", override: true},
      {:wampex, git: "https://gitlab.com/entropealabs/wampex.git", tag: "master", override: true},
      {:wampex_client, git: "https://gitlab.com/entropealabs/wampex_client.git", tag: "master"},
      {:wampex_router, git: "https://gitlab.com/entropealabs/wampex_router.git", tag: "master"}
    ]
  end

  defp deps(:release) do
    [
      {:cluster_kv, "~> 0.2", override: true},
      {:wampex, "~> 0.2", override: true},
      {:wampex_client, "~> 0.2"},
      {:wampex_router, "~> 0.2"}
    ]
  end

  defp aliases do
    [
      all_tests: [
        "compile --force --warnings-as-errors",
        "coveralls"
      ]
    ]
  end
end
