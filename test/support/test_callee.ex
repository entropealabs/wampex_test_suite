defmodule TestCallee do
  @moduledoc false
  use Wampex.Client.Handler
  @behaviour Wampex.Client.Handler
  require Logger

  @impl true
  def do_init(test: test, device: device), do: %{device: device, test: test, client_name: nil}

  @impl true
  def handle_continue({:registered, %{registrations: [id | _]}}, %{test: test} = state) do
    send(test, {:registered, id})
    {:noreply, state}
  end

  invocation("return.error", al, akw, state) do
    {:error, "this.is.an.error", al, akw, state}
  end

  invocation("com.actuator.as987d9a8sd79a87ds.light", al, akw, %{test: test} = state) do
    send(test, al)
    {:ok, [:ok], %{color: Map.get(akw, "color")}, state}
  end
end
