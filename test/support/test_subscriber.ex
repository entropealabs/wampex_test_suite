defmodule TestSubscriber do
  @moduledoc false
  use Wampex.Client.Handler
  @behaviour Wampex.Client.Handler
  require Logger

  @impl true
  def do_init(test: test, topic: topic), do: %{test: test, topic: topic, client_name: nil}

  def handle_continue({:registered, %{subscriptions: [id | _]}}, %{test: test} = state) do
    send(test, {:subscribed, id})
    {:noreply, state}
  end

  event("com.data.test.temp", al, _kw, %{test: test} = state) do
    send(test, al)
    {:noreply, state}
  end

  event("com.data.test", al, _kw, %{test: test} = state) do
    send(test, al)
    {:noreply, state}
  end

  event("com.data", al, _kw, %{test: test} = state) do
    send(test, al)
    {:noreply, state}
  end

  event({"com...temp", "wildcard"}, al, _kw, %{test: test} = state) do
    send(test, al)
    {:noreply, state}
  end

  event({"com..test.temp", "wildcard"}, al, _kw, %{test: test} = state) do
    send(test, al)
    {:noreply, state}
  end

  event("router.peer.disconnect", al, _kw, %{test: test} = state) do
    send(test, al)
    {:noreply, state}
  end
end
