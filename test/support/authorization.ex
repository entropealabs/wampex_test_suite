defmodule Wampex.Router.AuthorizationImpl do
  @moduledoc false
  @behaviour Wampex.Router.Authorization

  @impl true
  def authorized?(_, _, _), do: true
end
